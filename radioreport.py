
#import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib import style
#import seaborn as sns
import pandas as pd
from pprint import pprint as pprint
import os
import glob
# Make data
import fileListBuilder
import file_manip
import LTE_stat_XMLparse
from datetime import datetime













def getMimo(enodeb, startdatetime, enddatetime):
    with open('path.txt', 'r') as pathread:
        pathlist = pathread.readlines()
        path = pathlist[0][:-1]
        wpath = pathlist[1]
        wpath = wpath.rstrip()
        wpath = os.path.abspath(wpath)
    scounterList= ["VS.DLMimoEligibilityDecision.Eligible", "VS.DLMimoEligibilityDecision.NotEligible"]
    mimoDict = {}
    preList = fileListBuilder.identifyFiles(enodeb, startdatetime, enddatetime)


    file_manip.cleanupData(preList)
    file_manip.unzipList(preList)
    for stat in scounterList:



        fileList = []
        for file in glob.glob(os.path.join(wpath, '*')):
            fileList.append(file)
        moidList = LTE_stat_XMLparse.parseStat(fileList, stat)
        sectorList = []
        for item in moidList:
            item = item.split("EutranCell=")
            sectorList.append(item[1])

        with open("counter2.csv", "r") as counters:
            sectorDict = {}
            for sector in sectorList:
                sectIndex = sectorList.index(sector)
                sectIndex = sectIndex +1
                counters.seek(0)
                #sector_num = int(sector[-1:])
                lineDict = {}
                for line in counters:
                    print(line)
                    line = line.rstrip()
                    line = line.split(",")
                    datetime = line[0]
                    counter = line[sectIndex]
                    print(datetime, sector, counter)
                    lineDict[datetime] = counter
                sectorDict[sector] = lineDict
        mimoDict[stat] = sectorDict
    pprint(mimoDict)

    with open("cleanedvalues.csv", "w") as outfile:
        outfile.write("DateTime,Sector,Eligble,NotEligble\n")
        for sector in sorted(mimoDict["VS.DLMimoEligibilityDecision.Eligible"]):
            for period in sorted(mimoDict["VS.DLMimoEligibilityDecision.Eligible"][sector]):
                eligble = mimoDict["VS.DLMimoEligibilityDecision.Eligible"][sector][period]
                noteligble = mimoDict["VS.DLMimoEligibilityDecision.NotEligible"][sector][period]
                total = int(eligble) + int(noteligble)
                try:
                    percentEligble = int(eligble) / total
                    percentNot = 1 - percentEligble
                except:
                    percentEligble= 0
                    percentNot= 0
                outfile.write("{},{},{},{}\n".format(period, sector, percentEligble, percentNot))



    df = pd.read_csv("cleanedvalues.csv")
    print(df.head(3))
    Sectorcount = len(df.groupby("Sector"))
    if Sectorcount <= 4:
        srows = 2
        scols = 2
    else:
        srows = 2
        scols = 3
    style.use('ggplot')
    fig, axes = plt.subplots(figsize=(15, 8), nrows=srows, ncols=scols, sharex=False, sharey=True)
    #fig(figsize=(15, 8))
    axes_list = [item for sublist in axes for item in sublist]

    print(len(axes_list))
    x = 0
    for sectorName, selection in df.groupby("Sector"):
        leg = False
        x += 1
        if x == 1:
            leg = True
        selection['DateTime'] = pd.to_datetime(selection['DateTime'], format="%Y-%m-%d %H:%M:%S")
        print(selection)
        print(type(selection))
        ax = axes_list.pop(0)
        selection.plot.area(x="DateTime", y=["Eligble", "NotEligble"], ax=ax, stacked=True, legend=leg)

        ax.set_title(sectorName)
        ax.set_ylim((0, 1))
        plt.tight_layout()
    plt.show()
    emptyList = []
    file_manip.cleanupData(emptyList)

def main():
    enodeb = "100014_WCW_Tankersley"
    #startdatetime = "20180606 00:00:00"
    startdatetime = datetime.strptime(startdatetime, '%Y%m%d %H:%M:%S')
    #enddatetime = "20180606 05:00:00"
    enddatetime = datetime.strptime(enddatetime, '%Y%m%d %H:%M:%S')
    getMimo(enodeb, startdatetime, enddatetime)

if __name__ == '__main__':
    main()


#
# fig = plt.figure(figsize=(15, 8))
# style.use('ggplot')
# for sector in dataDict:
#     counter += 1
#     datelist = []
#     notmimolist = []
#     mimolist = []
#     for date, mim in dataDict[sector].items():
#         datelist.append(date)
#         notmimolist.append(mim)
#         mimolist.append(100-mim)
#     print("")
#     print(datelist)
#     print(mimolist)
#     fig.add_subplot(2,2,counter)
#     plt.subplots_adjust(hspace=0.8, wspace=0.8)
#     plt.stackplot(datelist,  mimolist,  notmimolist, labels=['Mimo','Diversity'])
#     plt.xticks(rotation=70)
#     plt.xlabel(sector)
#     plt.tight_layout()
#     plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.05), fancybox=True, shadow=True, ncol=2)
# plt.show()


# with open("cleanedvalues.csv", "w") as outfile:
#
#     sectorList = []
#     for sector in sorted(mimoDict["VS.DLMimoEligibilityDecision.Eligible"]):
#         sectorList.append(sector)
#     sectString = ",".join(sectorList)
#     outfile.write("DateTime,{}\n".format(",".join(sectorList)))
#     for period in mimoDict["VS.DLMimoEligibilityDecision.Eligible"][sectorList[0]]:
#         outlist = []
#         outlist.append(period)
#         for item in sectorList:
#             eligble = mimoDict["VS.DLMimoEligibilityDecision.Eligible"][item][period]
#             noteligble = mimoDict["VS.DLMimoEligibilityDecision.NotEligible"][item][period]
#             total = int(eligble) + int(noteligble)
#             percentEligble = int(eligble) / total
#             percentEligble = round((percentEligble), 2)
#             sectorint = int(sector[-1:])
#             outlist.append(str(percentEligble))
#         print(outlist)
#         outfile.write("{}\n".format(",".join(outlist)))

# df = pd.read_csv("cleanedvalues.csv")
# print(df.head(3))


#def getMimo():







# with open("menard_mimo.csv", "r") as infile:
#     dataDict = {}
#     infile.readline()
#     for line in infile:
#         dataDict
#         lineDict = {}
#         line = line.split(",")
#         cell = line[1]
#         date = line[3]
#         perMimo = line[4]
#         perMimo = perMimo.replace('%', '')
#         perMimo = float(perMimo)
#         #MomoNo = perMimo
#         MimoEl = 1 - perMimo
#         try:
#             dataDict[cell][date] = perMimo
#         except KeyError:
#             dataDict[cell] = {date: perMimo}
#     pprint(dataDict)
#
# counter = 0








#
# data = pd.DataFrame({  'group_A':[1,4,6,8,9], 'group_B':[2,24,7,10,12], 'group_C':[2,8,5,10,6], }, index=range(1,6))
#
# # We need to transform the data from raw data to percentage (fraction)
# data_perc = data.divide(data.sum(axis=1), axis=0)
#
# # Make the plot
# plt.stackplot(range(1,6),  data_perc["group_A"],  data_perc["group_B"],  data_perc["group_C"], labels=['A','B','C'])
# plt.legend(loc='upper left')
# plt.margins(0,0)
# plt.title('100 % stacked area chart')
# plt.show()
